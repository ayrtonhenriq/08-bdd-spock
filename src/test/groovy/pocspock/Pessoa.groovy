package pocspock

class Pessoa {
	
	String nome
	int idade
	
	static void main(args) {
		println("olá, groovy")
		def pessoinha = new Pessoa(nome:'Yoshi', idade: 37)
		
		println("O nome dele é ${pessoinha.nome}")
		
		pessoinha.setNome('José')
		
		println("O nome dele é ${pessoinha.getNome()}")
		
		println("""
			Seu amor me pegou
			Que tiro foi esse?
		""")
		
		def cores = ['azul', 'verde', 'branco']
		
		println(cores[0])
		println(cores[-1])
		
		println(cores.first())
		println(cores.last())
		
		cores.each {
			println("cor: ${it}")
		}
		
		cores.each { cor ->
			println("cor: ${cor}")
		}
		
		// exiba uma palavra qualquer 10x
		10.times {
			println("abacate")
		}
		
		
		if (cores) {
			println("cores tem algo")
		}
		
		def apelido = 'zeze'
		if (apelido) {
			println('existe apelido')
		}
		
		apelido = null
		if (apelido) {
			println('existe apelido')
		}
		
		def filhos = 5
		if (filhos) {
			println("tem filhos")
		}
		
		filhos = 0
		if (filhos) {
			println("tem filhos")
		}
		
		
		List bairros = null
		println(bairros?.first())

		bairros = ['cidade tiradentes', 'guaianazes']
		println(bairros?.first())
	}
}




